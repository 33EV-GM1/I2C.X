/*! \file  24FC128.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 4, 2015, 3:04 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef _24FC128_H
#define	_24FC128_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "I2C.h"
  
/*! read24FC128 - Read a byte from the 24FC128 */
unsigned char read24FC128( unsigned char, unsigned int );
/*! write24FC128 - Write a byte to the 24FC128 */
void write24FC128( unsigned char, unsigned int, unsigned char );
/*! writePage24FC128 - Write a page to the 24FC128 */
void writePage24FC128( unsigned char, unsigned int, unsigned char );

#ifdef	__cplusplus
}
#endif

#endif	/* _24FC128_H */

