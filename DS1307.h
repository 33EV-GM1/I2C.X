/*! \file  DS1307.h
 *
 *  \brief Header for DS1307 real-time clock
 * 
 * Contains function prototypes and manifest constants for
 * Maxxim/Dallas DS1307 real-time clock/calendar
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:48 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef DS1307_H
#define	DS1307_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "I2C.h"

/*! I2C address of chip */  
#define DS1307_ADDRESS 0x68

/*! DS1307readByte() - Read a byte from the DS1307. */
unsigned char DS1307readByte( unsigned char, unsigned int );




#ifdef	__cplusplus
}
#endif

#endif	/* DS1307_H */

