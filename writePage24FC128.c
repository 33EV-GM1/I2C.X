/*! \file  writePage24FC128.c
 *
 *  \brief Write an entire page to the EEPROM
 *
 *
 *  \author jjmcd
 *  \date January 15, 2016, 7:42 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#ifndef FCY
#define FCY 70000000
#endif
#include <libpic30.h>
#include "../include/I2C.h"


/*! writePage24FC128 - Write a page to the 24FC128 */
/*! writePage24FC128() sends a 64 byte page to the 24FC128 serial EEPROM.
 *
 *  \param ucDevice   unsigned char - 24FC128 device address
 *  \param uAddress   unsigned int  - Address of location to write
 *  \param szValue    unsigned char * - Value to be written
 *  \returns none
 */
void writePage24FC128( unsigned char ucDevice,
                         unsigned int  uAddress,
                         unsigned char *szValue )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr,ucHighAddr;
    int i;
    
    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit.  However, on the
     * DS1307 the control byte includes the high three bits of the
     * memory address as bits 1, 2, and 3, with bit 0 still 0 for write */
    ucControlByte = ( ucDevice /*& 0xf0*/ ) /*| ( (uAddress & 0x700)>>7 )*/;
    ucLowAddr = uAddress & 0xff;
    ucHighAddr = ( uAddress>>8 ) & 0xff;
    
    I2Cstart();         /* Start I2C transaction            */
    I2Cwrite(ucControlByte); /* Address of DS1307 | write  */
    I2Cwrite(ucHighAddr);
    I2Cwrite(ucLowAddr);/*  bits of value             */
    for ( i=0; i<64; i++ )
      {
        I2Cwrite(*szValue);  /* 8 bits of value              */    
        szValue++;
      }
    I2Cstop();          /* Stop the transaction             */
    __delay_ms(5);      /* 5ms write cycle                  */
}

