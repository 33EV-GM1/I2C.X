/*! \file  I2Cstart.c
 *
 *  \brief Generates an I2C Start Condition
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:56 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2Cstart() - Generates an I2C Start Condition.   */
/*! This function generates an I2C start condition and returns status
 *  of the Start.
 *
 *  \param none
 *  \return I2C1STATbits.S - Start condition detected
 */
unsigned int I2Cstart(void)
{
    I2Cidle();                      /* Wait for bus idle            */
    I2C1CON1bits.SEN = 1;             /* Generate Start Condition     */
    while (I2C1CON1bits.SEN);         /* Wait for Start Condition     */
    return(I2C1STATbits.S);          /* Optionally return status     */
}
