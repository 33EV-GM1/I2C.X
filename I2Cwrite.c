/*! \file  I2Cwrite.c
 *
 *  \brief Writes a byte out to the bus
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:35 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "I2C.h"

/*! I2Cwrite() - Writes a byte out to the bus   */
/*! This function transmits the byte passed to the function
 *
 *  \param byte (unsigned char) byte to be written
 *  \return none
 *
 */
void I2Cwrite(unsigned char byte)
{
    I2Cidle();                      /* Wait for bus to be idle          */
    I2C1TRN = byte;                  /* Load byte to I2C Transmit buffer */
    while (I2C1STATbits.TBF);        /* wait for data transmission       */
}
