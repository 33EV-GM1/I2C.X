/*! \file  DS1307readByte.c
 *
 *  \brief Read a byte from the DS1307 real-time clock/calendar
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:49 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1307.h"

/*! DS1307readByte() - Read a byte from the DS1307. */
/*! This function reads a byte from the DS1307 FRAM.
 *
 *  \param ucDevice   unsigned char - DS1307 device address
 *  \param uAddress   unsigned int  - Address of location to read
 *  \return unsigned char contents of memory cell
 *
 */
unsigned char DS1307readByte( unsigned char ucDevice,
                              unsigned int  uAddress )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr;
    unsigned char ucResult;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit. */
    ucControlByte = ( ucDevice<<1 );
    ucLowAddr = uAddress & 0xff;

    I2Cstart();                 /* Start I2C transaction            */
    I2Cwrite( ucControlByte );  /* Send bus Address                 */
    I2Cwrite(ucLowAddr);        /* Address of desired register      */
    I2Crestart();               /* Restart so can send read         */
    I2Cwrite( ucControlByte+1 );/* Send bus address with write      */
    ucResult = I2Cget();        /* Get answer from DS1307           */
    I2CnotACK();                /* NAK result to stop answers       */
    I2Cstop();                  /* Send stop on bus                 */

    return ( ucResult );
}
