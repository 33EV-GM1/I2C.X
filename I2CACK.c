/*! \file  I2CACK.c
 *
 *  \brief Generate I2C Acknowledge
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:43 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! I2CACK - Generate I2C Acknowledge */

/*! (unimplemented)
 *
 */
void I2CACK(void)
{

}
