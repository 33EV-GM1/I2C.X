/*! \file  I2CnotACK.c
 *
 *  \brief Generates a NO Acknowledge on the Bus
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 10:46 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "I2C.h"

/*! I2CnotACK() - Generates a NO Acknowledge on the Bus   */
/*! Sets the NO-ACK bit, then ACK enable and then waits for
 *  the NAK to complete.
 *
 *  \param none
 *  \return none
 */
void I2CnotACK(void)
{
    I2Cidle();
    I2C1CON1bits.ACKDT = 1;                   //Set for NotACk
    I2C1CON1bits.ACKEN = 1;
    while(I2C1CON1bits.ACKEN);                //wait for ACK to complete
    I2C1CON1bits.ACKDT = 0;                   //Set for NotACk
}
