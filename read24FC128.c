/*! \file  read24FC128.c
 *
 *  \brief  Read a byte from the 24FC128 serial EEPROM
 *
 *
 *  \author jjmcd
 *  \date November 4, 2015, 9:46 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/I2C.h"

/*! read24FC128 - Read a byte from the 24FC128 */
/*! This function reads a byte from the 24FC128 serial EEPROM.
 *
 *  \param ucDevice   unsigned char - 24FC128 device address
 *  \param uAddress   unsigned int  - Address of location to read
 *  \return unsigned char contents of memory cell
 *
 */
unsigned char read24FC128( unsigned char ucDevice,
                  unsigned int  uAddress )
{
    unsigned char ucControlByte;
    unsigned char ucLowAddr,ucHighAddr;
    unsigned char ucResult;

    /* Write condition is a zero bit so the control byte is formed
     * merely by shifting the address left one bit.  However, on the
     * DS1307 the control byte includes the high three bits of the
     * memory address as bits 1, 2, and 3, with bit 0 still 0 for write */
    ucControlByte = ( ucDevice  & 0xfe ) /*| ( (uAddress & 0x700)>>7 )*/;
    ucLowAddr = uAddress & 0xff;
    ucHighAddr = ( uAddress>>8 ) & 0xff;

    I2Cstart();                 /* Start I2C transaction            */
    I2Cwrite( ucControlByte );  /* Send bus Address                 */
    I2Cwrite(ucHighAddr);        /* Address of desired register      */
    I2Cwrite(ucLowAddr);        /* Address of desired register      */
    I2Crestart();               /* Restart so can send read         */
    I2Cwrite( ucControlByte+1 );/* Send bus address with write      */
    ucResult = I2Cget();        /* Get answer from DS1307        */
    I2CnotACK();                /* NAK result to stop answers       */
    I2Cstop();                  /* Send stop on bus                 */

    return ( ucResult );
}
